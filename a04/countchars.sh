#!/bin/sh

#countchars.sh
#Finished by: Phil Fiess, 100212874
#CPSC 1280

[ $# -ge 1 ] || {
	cat >&2 <<- EOF
		Pipe a text or string into the shell script, then enter a value to search!
	EOF
	exit 1;
}

# #reads occurrences of the character per line
while read i; do
echo "$2 : $(echo $i | grep -o "$2" | wc -l)"
done < $1

exit 0;
