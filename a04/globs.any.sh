#!/bin/sh

[ $# -ge 1 ] || {
	cat <<- EOF
		NAME
		globs.any.sh - displays entries in the current directory that match any of the given globs

		SYNOPSIS
		$(basename $0) glob[...]

		DESCRIPTION
		Displays the names in the current directory that match ANY of the given globs.
		Each file is displayed at most once.

		EXIT STATUS
		0 if any name matched any glob
		1 if no name matched any glob
		2 if no glob given

		NOTES
		use case in to do the glob matching

		EXAMPLES

		\$ $(basename $0) '*'
		123
		abc
		abc123
		xxx

		\$ $(basename $0) '*c*' '*1*' '*z*'
		123
		abc
		abc123

		ENTER A GLOB FOR THE DIRECTORY TO MATCH!
	EOF
	exit 2
} >&2


matchesTrue=$(ls -l $@ 2>/dev/null | wc -l)

case $matchesTrue in
	0)
		echo "No matches!"
		exit 1
		;;
	[!0])
		ls -1 $@ 2>/dev/null
		exit 0
		;;
	*)
		echo "Error. Try again"
		exit 2
		;;
esac

