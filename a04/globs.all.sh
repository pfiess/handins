#!/bin/sh

[ $# -ge 1 ] || {
	cat <<- EOF
		NAME
		globs.all.sh - displays entries in the current directory that match all of the given globs

		SYNOPSIS
		$(basename $0) glob[...]

		DESCRIPTION
		Displays the names in the current directory that match ALL of the given globs.
		Each file is displayed at most once.

		EXIT STATUS
		0 if any name matched all globs
		1 if no name matched all globs
		2 if no glob given

		NOTES
		use case in to do the glob matching

		EXAMPLES

		\$ $(basename $0) '*'
		123
		abc
		abc123
		xxx

		\$ $(basename $0) '*c*' '*1*'
		abc123

		ERROR: PROVIDE A GLOB TO MATCH IN DESIRED DIRECTORY

	EOF
	exit 2
} >&2


$DIRLIST=$(ls -1)
matchCount=0
output=""
#cycles through positional arguments and checks to see if each glob matches in the directory
for item in $DIRLIST 
do
	for arg in "$@"
	do
			case $item in 
			$arg)
				matchCount=1
				;;
			*)
				echo "no match"
				exit 1
				;;
			esac
	done

	if [ $matchCount -eq 1 ]; then 
	output=${output}$(echo "$item\n")
	fi
done

echo $(echo $output | uniq)

exit 0





